/** \file disassemble.h
 *  \author Peter H. Ezetta
 *  \version 0.1.0
 *  \date 01/12/2018
 *  \brief Prototypes for disassemble.c
 */

#ifndef DISASSEMBLE_H
#define DISASSEMBLE_H

#include "config.h"

#include "z80disasm.h"

int disassemble(unsigned char *buffer, int pc, struct todo_list *todo);

#endif
