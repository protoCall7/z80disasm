/** \file z80disasm.h
 *  \author Peter H. Ezetta
 *  \version 0.1.0
 *  \date 01/12/2018
 *  \brief Prototypes and datastructures for z80disasm.c
 */

#ifndef Z80DISASM_H
#define Z80DISASM_H

#include "config.h"

#include <stdbool.h>
#include <stdint.h>

#if HAVE_SYS_QUEUE
# include <sys/queue.h>
#endif

/** Element in a linked list of memory addresses */
typedef struct _Element {
	LIST_ENTRY(_Element) pointers; /**< linked list entry */
	uint16_t address; /**< memory address */
} element;

LIST_HEAD(todo_list, _Element);
LIST_HEAD(done_list, _Element);

element *new_element(uint16_t address);

#endif
