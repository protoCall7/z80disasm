/**
 * \file z80disasm.c
 * \brief A recursive descent z80 disassembler
 * \version 0.1.1
 * \date 11/24/2016
 * \author Peter H. Ezetta <protocall7@gmail.com>
 * \todo Implement getopt argument parsing
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdbool.h>

#if HAVE_ERR
# include <err.h>
#endif

#include <errno.h>

#if HAVE_SYS_QUEUE
# include <sys/queue.h>
#endif

#include "disassemble.h"

static bool search_done(uint16_t pc);
	
/**
 * linked list containing addresses to disassemble
 */
struct todo_list todo;

/**
 * linked list containing addresses already disassembled
 */
struct done_list done;

/**
 * \brief The entry point of the z80disasm program.
 *
 * This function parses command-line arguments, disassembles a Z80 binary file, and
 * writes the disassembly output to a file.
 *
 * \param[in] argc The number of command-line arguments.
 * \param[in] argv The array of command-line argument strings.
 *
 * \returns The exit status of the program (0 for success, non-zero for failure).
 */
int main(int argc, char *argv[])
{
	int fd;
	unsigned char *buffer;
	size_t filesize, pc;
	struct stat sb;

	/* setup */
#if HAVE_PLEDGE
	if (pledge("stdio rpath", NULL) == -1)
		err(1, NULL);
#endif

	if (argc != 2)
		errx(1, "Usage: ./z80disasm <filename>");

	if ((fd = open(argv[1], O_RDONLY)) < 0)
		err(1, "%s", argv[1]);

	if (fstat(fd, &sb) == -1) {
		close(fd);
		err(1, NULL);
	}

	filesize = sb.st_size;
	if (filesize == 0) {
		close(fd);
		errx(1, "File is empty, nothing to disassemble.");
	}

	buffer = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0);
	if (buffer == MAP_FAILED) {
		close(fd);
		err(1, NULL);
	}

	/* don't need the file pointer anymore */
	if (close(fd) == -1)
		err(1, NULL);

	LIST_INIT(&todo);
	LIST_INIT(&done);

	element *first_todo = new_element(0x0000);
	LIST_INSERT_HEAD(&todo, first_todo, pointers);

	/* begin disassembly */
	do {
		element *tmp = LIST_FIRST(&todo);
		pc = tmp->address;
		LIST_REMOVE(tmp, pointers);
		free(tmp);

		if (search_done(pc)) {
			continue;
		}

		printf("====== 0x%04zX ======\n", pc);
		while (pc < filesize) {
			element *new_addr = new_element(pc);
			LIST_INSERT_HEAD(&done, new_addr, pointers);
			int retval = disassemble(buffer, pc, &todo);
			if (retval < 0)
				break;

			pc += retval;
		}
		printf("\n");
	} while (!LIST_EMPTY(&todo));

	/* cleanup */
	if (munmap(buffer, filesize) == -1)
		err(1, NULL);

	/* list cleanup */
	while(!LIST_EMPTY(&done)) {
		element *d = LIST_FIRST(&done);
		LIST_REMOVE(d, pointers);
		free(d);
	}

	return 0;
}

/**
 * \brief Create a new instruction element.
 *
 * This function allocates memory for a new instruction element and initializes
 * its fields.
 *
 * \param[in] address The address of the new instruction.
 *
 * \returns A pointer to the new instruction element, or exits the program if an error occurred.
 */
element* new_element(uint16_t address)
{
	// Allocate memory for the new element
	element *e = calloc(1, sizeof(element));
	if (!e)
		err(1, "Failed to allocate memory for new instruction element");

	// Initialize the fields of the new element
	e->address = address;

	return e;
}


/**
 * \brief Searches the `done` linked list for a program counter address.
 *
 * This function searches the `done` linked list for the specified program counter
 * address. It returns true if the address is found in the linked list, and false
 * otherwise.
 *
 * \param[in] pc The program counter address to search for.
 * \returns true if `pc` appears in `done`, otherwise false.
 */
static bool search_done(uint16_t pc)
{
	if (LIST_EMPTY(&done))
		return false;

	// Iterate through the linked list to search for the specified address
	element *e;
	LIST_FOREACH(e, &done, pointers) {
		if (pc == e->address)
			return true;
	}

	return false;
}
