z80disasm is a binary disassembler for the Z80 CPU. It supports the complete instruction set, and utilizes a linked list queue to keep track of calls and jumps, providing a more complete disassembly than a linear sweep disassembler.

## Getting Started
z80disasm takes the path to a Z80 binary file as it's only argument, and outputs the assembly dump to STDOUT.

```shell
./z80disasm <path to binary>
```

## Building
z80disasm is built with BSD make, and utilizes oconfigure. The disassembler can be built and installed with the following commands

```shell
git clone https://gitlab.com/protoCall7/z80disasm.git
cd z80disasm/
./configure
make
sudo make install
```

## Contributing
If you'd like to contribute, please fork the repository and use a feature branch. Merge requests are warmly welcome.

## License
z80disasm is licensed under the BSD 3-Clause license. Please see the LICENSE file for more information.
