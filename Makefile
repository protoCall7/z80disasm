.POSIX:

include Makefile.configure

OBJS=compats.o disassemble.o z80disasm.o

all: z80disasm
compats.o: compats.c
disassemble.o: disassemble.c disassemble.h z80disasm.h
z80disasm.o: z80disasm.c z80disasm.h disassemble.h
z80disasm: ${OBJS}
	${CC} ${CFLAGS} -o $@ ${OBJS}
doc:
	doxygen
install: z80disasm
	${INSTALL_PROGRAM} z80disasm ${BINDIR}
	${INSTALL_MAN} z80disasm.1 ${MANDIR}
clean:
	rm -rf z80disasm *.dSYM *.log *.o docs
distclean: clean
	rm -f config.* Makefile.configure

.PHONY: all clean doc
