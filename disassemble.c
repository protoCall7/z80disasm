/** \file disassemble.c
 *  \author Peter H. Ezetta
 *  \version 0.1.3
 *  \date 05/17/2022
 *  \brief Functions for parsing the Z80 instruction set
 */

#include "config.h"

#include <stdio.h>

#include "disassemble.h"

static void disassemble_extended(unsigned char *opcode, int *opbytes, unsigned int *im);
static void disassemble_ix(unsigned char *opcode, int *opbytes);
static void disassemble_ix_bits(unsigned char *opcode, int *opbytes);
static void disassemble_bits(unsigned char *opcode);
static void disassemble_iy(unsigned char *opcode, int *opbytes);
static void disassemble_iy_bits(unsigned char *opcode, int *opbytes);

/**
 *  \brief Disassemble the mnemonic in buffer which is pointed to by pc
 *  \param[in] buffer pointer to buffer containing binary to disassemble
 *  \param[in] pc program counter to begin disassembly at
 *  \param[in,out] todo pointer to todo linked-list
 *  \return length of disassembled opcode in bytes
 */

int disassemble(unsigned char *buffer, int pc, struct todo_list *todo) {
	unsigned char *opcode = &buffer[pc];
	int opbytes = 1;
	unsigned int im = 0;

	printf("%04X    ", pc);
	switch(*opcode) {
	case 0x00:
		printf("NOP");
		break;
	case 0x01:
		printf("LD   BC,%02X%02X", opcode[2], opcode[1]);
		opbytes += 2;
		break;
	case 0x02:
		printf("LD   (BC),A");
		break;
	case 0x03:
		printf("INC  BC");
		break;
	case 0x04:
		printf("INC  B");
		break;
	case 0x05:
		printf("DEC  B");
		break;
	case 0x06:
		printf("LD   B,%02X", opcode[1]);
		opbytes++;
		break;
	case 0x07:
		printf("RLCA");
		break;
	case 0x08:
		printf("EX   AF,AF'");
		break;
	case 0x09:
		printf("ADD  HL,BC");
		break;
	case 0x0A:
		printf("LD   A,(BC)");
		break;
	case 0x0B:
		printf("DEC  BC");
		break;
	case 0x0C:
		printf("INC  C");
		break;
	case 0x0D:
		printf("DEC  C");
		break;
	case 0x0E:
		printf("LD   C,%02X", opcode[1]);
		opbytes++;
		break;
	case 0x0F:
		printf("RRCA");
		break;
	case 0x10:
		printf("DJNZ %02X", opcode[1]);
		opbytes++;
		break;
	case 0x11:
		printf("LD   DE,%02X%02X", opcode[2], opcode[1]);
		opbytes += 2;
		break;
	case 0x12:
		printf("LD   DE,A");
		break;
	case 0x13:
		printf("INC  DE");
		break;
	case 0x14:
		printf("INC  D");
		break;
	case 0x15:
		printf("DEC  D");
		break;
	case 0x16:
		printf("LD   D,%02X", opcode[1]);
		opbytes++;
		break;
	case 0x17:
		printf("RLA");
		break;
	case 0x18:
		printf("JR   %02X", opcode[1]);
		opbytes++;
		break;
	case 0x19:
		printf("ADD  HL,DE");
		break;
	case 0x1A:
		printf("LD   A,DE");
		break;
	case 0x1B:
		printf("DEC  DE");
		break;
	case 0x1C:
		printf("INC  E");
		break;
	case 0x1D:
		printf("DEC  E");
		break;
	case 0x1E:
		printf("LD   E,%02X", opcode[1]);
		opbytes++;
		break;
	case 0x1F:
		printf("RRA");
		break;
	case 0x20:
		printf("JR   NZ,%02X", opcode[1]);
		opbytes++;
		break;
	case 0x21:
		printf("LD   HL,%02X%02X", opcode[2], opcode[1]);
		opbytes += 2;
		break;
	case 0x22:
		printf("LD   (%02X%02X),HL", opcode[2], opcode[1]);
		opbytes += 2;
		break;
	case 0x23:
		printf("INC  HL");
		break;
	case 0x24:
		printf("INC  H");
		break;
	case 0x25:
		printf("DEC  H");
		break;
	case 0x26:
		printf("LD   H,%02X", opcode[1]);
		opbytes++;
		break;
	case 0x27:
		printf("DAA");
		break;
	case 0x28:
		printf("JR   Z,%02X", opcode[1]);
		opbytes++;
		break;
	case 0x29:
		printf("ADD  HL,HL");
		break;
	case 0x2A:
		printf("LD   HL,(%02X%02X)", opcode[2], opcode[1]);
		opbytes += 2;
		break;
	case 0x2B:
		printf("DEC  HL");
		break;
	case 0x2C:
		printf("INC  L");
		break;
	case 0x2D:
		printf("DEC  L");
		break;
	case 0x2E:
		printf("LD   L,%02X", opcode[1]);
		opbytes++;
		break;
	case 0x2F:
		printf("CPL");
		break;
	case 0x30:
		printf("JR   NC,%02X", opcode[1]);
		opbytes++;
		break;
	case 0x31:
		printf("LD   SP,%02X%02X", opcode[2], opcode[1]);
		opbytes += 2;
		break;
	case 0x32:
		printf("LD   (%02X%02X),A", opcode[2], opcode[1]);
		opbytes += 2;
		break;
	case 0x33:
		printf("INC  SP");
		break;
	case 0x34:
		printf("INC  (HL)");
		break;
	case 0x35:
		printf("DEC  (HL)");
		break;
	case 0x36:
		printf("LD   (HL),%02X", opcode[1]);
		opbytes++;
		break;
	case 0x37:
		printf("SCF");
		break;
	case 0x38:
		printf("JR   C,%02X", opcode[1]);
		opbytes++;
		break;
	case 0x39:
		printf("ADD  HL,SP");
		break;
	case 0x3A:
		printf("LD   A,(%02X%02X)", opcode[2], opcode[1]);
		opbytes += 2;
		break;
	case 0x3B:
		printf("DEC  SP");
		break;
	case 0x3C:
		printf("INC  A");
		break;
	case 0x3D:
		printf("DEC  A");
		break;
	case 0x3E:
		printf("LD   A,%02X", opcode[1]);
		opbytes++;
		break;
	case 0x3F:
		printf("CCF");
		break;
	case 0x40:
		printf("LD   B,B");
		break;
	case 0x41:
		printf("LD   B,C");
		break;
	case 0x42:
		printf("LD   B,D");
		break;
	case 0x43:
		printf("LD   B,E");
		break;
	case 0x44:
		printf("LD   B,H");
		break;
	case 0x45:
		printf("LD   B,L");
		break;
	case 0x46:
		printf("LD   B,(HL)");
		break;
	case 0x47:
		printf("LD   B,A");
		break;
	case 0x48:
		printf("LD   C,B");
		break;
	case 0x49:
		printf("LD   C,C");
		break;
	case 0x4A:
		printf("LD   C,D");
		break;
	case 0x4B:
		printf("LD   C,E");
		break;
	case 0x4C:
		printf("LD   C,H");
		break;
	case 0x4D:
		printf("LD   C,L");
		break;
	case 0x4E:
		printf("LD   C,(HL)");
		break;
	case 0x4F:
		printf("LD   C,A");
		break;
	case 0x50:
		printf("LD   D,B");
		break;
	case 0x51:
		printf("LD   D,C");
		break;
	case 0x52:
		printf("LD   D,D");
		break;
	case 0x53:
		printf("LD   D,E");
		break;
	case 0x54:
		printf("LD   D,H");
		break;
	case 0x55:
		printf("LD   D,L");
		break;
	case 0x56:
		printf("LD   D,(HL)");
		break;
	case 0x57:
		printf("LD   D,A");
		break;
	case 0x58:
		printf("LD   E,B");
		break;
	case 0x59:
		printf("LD   E,C");
		break;
	case 0x5A:
		printf("LD   E,D");
		break;
	case 0x5B:
		printf("LD   E,E");
		break;
	case 0x5C:
		printf("LD   E,H");
		break;
	case 0x5D:
		printf("LD   E,L");
		break;
	case 0x5E:
		printf("LD   E,(HL)");
		break;
	case 0x5F:
		printf("LD   E,A");
		break;
	case 0x60:
		printf("LD   H,B");
		break;
	case 0x61:
		printf("LD   H,C");
		break;
	case 0x62:
		printf("LD   H,D");
		break;
	case 0x63:
		printf("LD   H,E");
		break;
	case 0x64:
		printf("LD   H,H");
		break;
	case 0x65:
		printf("LD   H,L");
		break;
	case 0x66:
		printf("LD   H,(HL)");
		break;
	case 0x67:
		printf("LD   H,A");
		break;
	case 0x68:
		printf("LD   L,B");
		break;
	case 0x69:
		printf("LD   L,C");
		break;
	case 0x6A:
		printf("LD   L,D");
		break;
	case 0x6B:
		printf("LD   L,E");
		break;
	case 0x6C:
		printf("LD   L,H");
		break;
	case 0x6D:
		printf("LD   L,L");
		break;
	case 0x6E:
		printf("LD   L,(HL)");
		break;
	case 0x6F:
		printf("LD   L,A");
		break;
	case 0x70:
		printf("LD   (HL),B");
		break;
	case 0x71:
		printf("LD   (HL),C");
		break;
	case 0x72:
		printf("LD   (HL),D");
		break;
	case 0x73:
		printf("LD   (HL),E");
		break;
	case 0x74:
		printf("LD   (HL),H");
		break;
	case 0x75:
		printf("LD   (HL),L");
		break;
	case 0x76:
		printf("HALT");
		opbytes = -1;
		break;
	case 0x77:
		printf("LD   (HL),A");
		break;
	case 0x78:
		printf("LD   A,B");
		break;
	case 0x79:
		printf("LD   A,C");
		break;
	case 0x7A:
		printf("LD   A,D");
		break;
	case 0x7B:
		printf("LD   A,E");
		break;
	case 0x7C:
		printf("LD   A,H");
		break;
	case 0x7D:
		printf("LD   A,L");
		break;
	case 0x7E:
		printf("LD   A,(HL)");
		break;
	case 0x7F:
		printf("LD   A,A");
		break;
	case 0x80:
		printf("ADD  A,B");
		break;
	case 0x81:
		printf("ADD  A,C");
		break;
	case 0x82:
		printf("ADD  A,D");
		break;
	case 0x83:
		printf("ADD  A,E");
		break;
	case 0x84:
		printf("ADD  A,H");
		break;
	case 0x85:
		printf("ADD  A,L");
		break;
	case 0x86:
		printf("ADD  A,(HL)");
		break;
	case 0x87:
		printf("ADD  A,A");
		break;
	case 0x88:
		printf("ADC  A,B");
		break;
	case 0x89:
		printf("ADC  A,C");
		break;
	case 0x8A:
		printf("ADC  A,D");
		break;
	case 0x8B:
		printf("ADC  A,E");
		break;
	case 0x8C:
		printf("ADC  A,H");
		break;
	case 0x8D:
		printf("ADC  A,L");
		break;
	case 0x8E:
		printf("ADC  A,(HL)");
		break;
	case 0x8F:
		printf("ADC  A,A");
		break;
	case 0x90:
		printf("SUB  B");
		break;
	case 0x91:
		printf("SUB  C");
		break;
	case 0x92:
		printf("SUB  D");
		break;
	case 0x93:
		printf("SUB  E");
		break;
	case 0x94:
		printf("SUB  H");
		break;
	case 0x95:
		printf("SUB  L");
		break;
	case 0x96:
		printf("SUB  (HL)");
		break;
	case 0x97:
		printf("SUB  A");
		break;
	case 0x98:
		printf("SBC  A,B");
		break;
	case 0x99:
		printf("SBC  A,C");
		break;
	case 0x9A:
		printf("SBC  A,D");
		break;
	case 0x9B:
		printf("SBC  A,H");
		break;
	case 0x9C:
		printf("SBC  A,L");
		break;
	case 0x9E:
		printf("SBC  A,(HL)");
		break;
	case 0x9F:
		printf("SBC  A,A");
		break;
	case 0xA0:
		printf("AND  B");
		break;
	case 0xA1:
		printf("AND  C");
		break;
	case 0xA2:
		printf("AND  D");
		break;
	case 0xA3:
		printf("AND  E");
		break;
	case 0xA4:
		printf("AND  H");
		break;
	case 0xA5:
		printf("AND  L");
		break;
	case 0xA6:
		printf("AND  (HL)");
		break;
	case 0xA7:
		printf("AND  A");
		break;
	case 0xA8:
		printf("XOR  B");
		break;
	case 0xA9:
		printf("XOR  C");
		break;
	case 0xAA:
		printf("XOR  D");
		break;
	case 0xAB:
		printf("XOR  E");
		break;
	case 0xAC:
		printf("XOR  H");
		break;
	case 0xAD:
		printf("XOR  L");
		break;
	case 0xAE:
		printf("XOR  (HL)");
		break;
	case 0xAF:
		printf("XOR  A");
		break;
	case 0xB0:
		printf("OR   B");
		break;
	case 0xB1:
		printf("OR   C");
		break;
	case 0xB2:
		printf("OR   D");
		break;
	case 0xB3:
		printf("OR   E");
		break;
	case 0xB4:
		printf("OR   H");
		break;
	case 0xB5:
		printf("OR   L");
		break;
	case 0xB6:
		printf("OR   (HL)");
		break;
	case 0xB7:
		printf("OR   A");
		break;
	case 0xB8:
		printf("CP   B");
		break;
	case 0xB9:
		printf("CP   C");
		break;
	case 0xBA:
		printf("CP   D");
		break;
	case 0xBB:
		printf("CP   E");
		break;
	case 0xBC:
		printf("CP   H");
		break;
	case 0xBD:
		printf("CP   L");
		break;
	case 0xBE:
		printf("CP   (HL)");
		break;
	case 0xBF:
		printf("CP   A");
		break;
	case 0xC0:
		printf("RET  NZ");
		break;
	case 0xC1:
		printf("POP  BC");
		break;
	case 0xC2:
		printf("JP   NZ,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xC3:
		printf("JP   %02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes = -1;
		}
		break;
	case 0xC4:
		printf("CALL NZ,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xC5:
		printf("PUSH BC");
		break;
	case 0xC6:
		printf("ADD  A,%02X", opcode[1]);
		opbytes++;
		break;
	case 0xC7:
		printf("RST  00h");
		{
			element *a1 = new_element(0x0000);
			LIST_INSERT_HEAD(todo, a1, pointers);
		}
		opbytes = -1;
		break;
	case 0xC8:
		printf("RET  Z");
		break;
	case 0xC9:
		printf("RET");
		opbytes = -1;
		break;
	case 0xCA:
		printf("JP   Z,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xCB:
		disassemble_bits(&opcode[1]);
		opbytes++;
		break;
	case 0xCC:
		printf("CALL Z,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xCD:
		printf("CALL %02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xCE:
		printf("ADC  A,%02X", opcode[2]);
		opbytes++;
		break;
	case 0xCF:
		printf("RST  08h");
		{
			element *a1 = new_element(0x0008);
			LIST_INSERT_HEAD(todo, a1, pointers);
		}
		opbytes = -1;
		break;
	case 0xD0:
		printf("RET  NC");
		break;
	case 0xD1:
		printf("POP  BC");
		break;
	case 0xD2:
		printf("JP   NC,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xD3:
		printf("OUT  (%02X),A", opcode[1]);
		opbytes++;
		break;
	case 0xD4:
		printf("CALL NC,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xD5:
		printf("PUSH DE");
		break;
	case 0xD6:
		printf("SUB  %02X", opcode[1]);
		opbytes++;
		break;
	case 0xD7:
		printf("RST  10h");
		{
			element *a1 = new_element(0x0010);
			LIST_INSERT_HEAD(todo, a1, pointers);
		}
		opbytes = -1;
		break;
	case 0xD8:
		printf("RET  C");
		break;
	case 0xD9:
		printf("EXX");
		break;
	case 0xDA:
		printf("JP   C,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xDB:
		printf("IN   A,(%02X)", opcode[1]);
		opbytes++;
		break;
	case 0xDC:
		printf("CALL C,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xDD:
		disassemble_ix(&opcode[1], &opbytes);
		opbytes++;
		break;
	case 0xDE:
		printf("SBC  A,%02X", opcode[1]);
		opbytes++;
		break;
	case 0xDF:
		printf("RST  18h");
		{
			element *a1 = new_element(0x0018);
			LIST_INSERT_HEAD(todo, a1, pointers);
		}
		opbytes = -1;
		break;
	case 0xE0:
		printf("RET  PO");
		break;
	case 0xE1:
		printf("POP  HL");
		break;
	case 0xE2:
		printf("JP   PO,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xE3:
		printf("EX   (SP),HL");
		break;
	case 0xE4:
		printf("CALL PO,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xE5:
		printf("PUSH HL");
		break;
	case 0xE6:
		printf("AND  %02X", opcode[1]);
		opbytes++;
		break;
	case 0xE7:
		printf("RST  20h");
		{
			element *a1 = new_element(0x0020);
			LIST_INSERT_HEAD(todo, a1, pointers);
		}
		opbytes = -1;
		break;
	case 0xE8:
		printf("RET  PE");
		break;
	case 0xE9:
		printf("JP   (HL)");
		break;
	case 0xEA:
		printf("JP   PE,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xEB:
		printf("EX   DE,HL");
		break;
	case 0xEC:
		printf("CALL PE,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xED:
		disassemble_extended(&opcode[1], &opbytes, &im);
		opbytes++;
		break;
	case 0xEE:
		printf("XOR  %02X", opcode[1]);
		opbytes++;
		break;
	case 0xEF:
		printf("RST  28h");
		{
			element *a1 = new_element(0x0028);
			LIST_INSERT_HEAD(todo, a1, pointers);
		}
		opbytes = -1;
		break;
	case 0xF0:
		printf("RET  P");
		break;
	case 0xF1:
		printf("POP  AF");
		break;
	case 0xF2:
		printf("JP   P,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xF3:
		printf("DI");
		break;
	case 0xF4:
		printf("CALL P,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xF5:
		printf("PUSH AF");
		break;
	case 0xF6:
		printf("OR   %02X", opcode[1]);
		opbytes++;
		break;
	case 0xF7:
		printf("RST  30h");
		{
			element *a1 = new_element(0x0030);
			LIST_INSERT_HEAD(todo, a1, pointers);
		}
		opbytes = -1;
		break;
	case 0xF8:
		printf("RET  M");
		break;
	case 0xF9:
		printf("LD   SP,HL");
		break;
	case 0xFA:
		printf("JP   M,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xFB:
		printf("EI");
		break;
	case 0xFC:
		printf("CALL M,%02X%02X", opcode[2], opcode[1]);
		{
			int offset = opcode[1] | (opcode[2] << 8);
			element *a1 = new_element(offset);
			LIST_INSERT_HEAD(todo, a1, pointers);
			opbytes += 2;
		}
		break;
	case 0xFD:
		disassemble_iy(&opcode[1], &opbytes);
		opbytes++;
		break;
	case 0xFE:
		printf("CP   %02X", opcode[1]);
		opbytes++;
		break;
	case 0xFF:
		printf("RST  38h");
		{
			element *a1 = new_element(0x0038);
			LIST_INSERT_HEAD(todo, a1, pointers);
		}
		opbytes = -1;
		break;
	}
	printf("\n");

	if (im == 1) {
		element *a1 = new_element(0x0038);
		LIST_INSERT_HEAD(todo, a1, pointers);
	}

	return opbytes;
}

/** \brief Disassemble the extended instructions
 *  \param[in] opcode pointer to opcode to disassemble
 *  \param[out] opbytes pointer to int containing length of disassembled opcode
 *  \param[out] im pointer to an int for storing the interrupt mode
 */

static void disassemble_extended(unsigned char *opcode, int *opbytes,
                          unsigned int *im) {
	switch(*opcode) {
	case 0x40:
		printf("IN   B,(C)");
		break;
	case 0x41:
		printf("OUT  (C),B");
		break;
	case 0x42:
		printf("SBC  HL,BC");
		break;
	case 0x43:
		printf("LD   (%02X%02X),BC", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x44:
		printf("NEG");
		break;
	case 0x45:
		printf("RETN");
		break;
	case 0x46:
		printf("IM   0");
		*im = 0;
		break;
	case 0x47:
		printf("LD   I,A");
		break;
	case 0x48:
		printf("IN   C,(C)");
		break;
	case 0x49:
		printf("OUT  (C),C");
		break;
	case 0x4A:
		printf("ADC  HL,BC");
		break;
	case 0x4B:
		printf("LD   BC,(%02X%02X)", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x4C:
		printf("NEG");
		break;
	case 0x4D:
		printf("RETI");
		*opbytes = -1;
		break;
	case 0x4E:
		printf("IM   0/1");
		*im = 0;
		break;
	case 0x4F:
		printf("LD   R,A");
		break;
	case 0x50:
		printf("IN   D,(C)");
		break;
	case 0x51:
		printf("OUT  (C),D");
		break;
	case 0x52:
		printf("SBC  HL,DE");
		break;
	case 0x53:
		printf("LD   (%02X%02X),DE", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x54:
		printf("NEG");
		break;
	case 0x55:
		printf("RETN");
		break;
	case 0x56:
		printf("IM   1");
		*im = 1;
		break;
	case 0x57:
		printf("LD   A,I");
		break;
	case 0x58:
		printf("IN   E,(C)");
		break;
	case 0x59:
		printf("OUT  (C),E");
		break;
	case 0x5A:
		printf("ADC  HL,DE");
		break;
	case 0x5B:
		printf("LD   DE,(%02X%02X)", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x5C:
		printf("NEG");
		break;
	case 0x5D:
		printf("RETN");
		break;
	case 0x5E:
		printf("IM   2");
		*im = 2;
		break;
	case 0x5F:
		printf("LD   A,R");
		break;
	case 0x60:
		printf("IN   H,(C)");
		break;
	case 0x61:
		printf("OUT  (C),H");
		break;
	case 0x62:
		printf("SBC  HL,HL");
		break;
	case 0x63:
		printf("LD   (%02X%02X),HL", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x64:
		printf("NEG");
		break;
	case 0x65:
		printf("RETN");
		break;
	case 0x66:
		printf("IM   0");
		*im = 0;
		break;
	case 0x67:
		printf("RRD");
		break;
	case 0x68:
		printf("IN   L,(C)");
		break;
	case 0x69:
		printf("OUT  (C),L");
		break;
	case 0x6A:
		printf("ADC  HL,HL");
		break;
	case 0x6B:
		printf("LD   HL,(%02X%02X)", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x6C:
		printf("NEG");
		break;
	case 0x6D:
		printf("RETN");
		break;
	case 0x6E:
		printf("IM   0/1");
		*im = 0;
		break;
	case 0x6F:
		printf("RLD");
		break;
	case 0x70:
		printf("IN   C");
		break;
	case 0x71:
		printf("OUT  (C),0");
		break;
	case 0x72:
		printf("SBC  HL,SP");
		break;
	case 0x73:
		printf("LD   (%02X%02X),SP", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x74:
		printf("NEG");
		break;
	case 0x75:
		printf("RETN");
		break;
	case 0x76:
		printf("IM   1");
		*im = 1;
		break;
	case 0x78:
		printf("IN   A,(C)");
		break;
	case 0x79:
		printf("OUT  (C),A");
		break;
	case 0x7A:
		printf("ADC  HL,SP");
		break;
	case 0x7B:
		printf("LD   SP,(%02X%02X)", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x7C:
		printf("NEG");
		break;
	case 0x7D:
		printf("RETN");
		break;
	case 0x7E:
		printf("IM   2");
		*im = 2;
		break;
	case 0xA0:
		printf("LDI");
		break;
	case 0xA1:
		printf("CPI");
		break;
	case 0xA2:
		printf("INI");
		break;
	case 0xA3:
		printf("OUTI");
		break;
	case 0xA8:
		printf("LDD");
		break;
	case 0xA9:
		printf("CPD");
		break;
	case 0xAA:
		printf("IND");
		break;
	case 0xAB:
		printf("OUTD");
		break;
	case 0xB0:
		printf("LDIR");
		break;
	case 0xB1:
		printf("CPIR");
		break;
	case 0xB2:
		printf("INIR");
		break;
	case 0xB3:
		printf("OTIR");
		break;
	case 0xB8:
		printf("LDDR");
		break;
	case 0xB9:
		printf("CPDR");
		break;
	case 0xBA:
		printf("INDR");
		break;
	case 0xBB:
		printf("OTDR");
		break;
	default:
		printf("Invalid instruction...");
		*opbytes = -1;
		break;
	}
}

/** \brief Disassemble the IX instructions
 *  \param[in] opcode pointer to opcode to disassemble
 *  \param[out] opbytes pointer to int containing length of disassembled opcode
 */

static void disassemble_ix(unsigned char *opcode, int *opbytes) {
	switch(*opcode) {            /* these are the IX instructions */
	case 0x09:
		printf("ADD  IX,BC");
		break;
	case 0x19:
		printf("ADD  IX,DE");
		break;
	case 0x21:
		printf("LD   IX,%02X%02X", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x22:
		printf("LD   (%02X%02X),IX", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x23:
		printf("INC  IX");
		break;
	case 0x24:
		printf("INC  IXH");
		break;
	case 0x25:
		printf("DEC  IXH");
		break;
	case 0x26:
		printf("LD   IXH,%02X", opcode[1]);
		*opbytes += 1;
		break;
	case 0x29:
		printf("ADD  IX,IX");
		break;
	case 0x2A:
		printf("LD   IX,(%02X%02X)", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x2B:
		printf("INC  IXL");
		break;
	case 0x2C:
		printf("DEC  IXL");
		break;
	case 0x2D:
		printf("LD   IXL,%02X", opcode[1]);
		*opbytes += 1;
		break;
	case 0x34:
		printf("INC  (IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x35:
		printf("DEC  (IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x36:
		printf("LD   (IX+%02X), %02X", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x39:
		printf("ADD  IX,SP");
		break;
	case 0x44:
		printf("LD   B,IXH");
		break;
	case 0x45:
		printf("LD   B,IXL");
		break;
	case 0x46:
		printf("LD   B,(IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x4C:
		printf("LD   C,IXH");
		break;
	case 0x4D:
		printf("LD   C,IXL");
		break;
	case 0x4E:
		printf("LD   C,(IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x54:
		printf("LD   D,IXH");
		break;
	case 0x55:
		printf("LD   D,IXL");
		break;
	case 0x56:
		printf("LD   D,(IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x5C:
		printf("LD   E,IXH");
		break;
	case 0x5D:
		printf("LD   E,IXL");
		break;
	case 0x5E:
		printf("LD   E,(IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x60:
		printf("LD   IXH,B");
		break;
	case 0x61:
		printf("LD   IXH,C");
		break;
	case 0x62:
		printf("LD   IXH,D");
		break;
	case 0x63:
		printf("LD   IXH,E");
		break;
	case 0x64:
		printf("LD   IXH,IXH");
		break;
	case 0x65:
		printf("LD   IXH,IXL");
		break;
	case 0x66:
		printf("LD   H,(IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x67:
		printf("LD   IXH,A");
		break;
	case 0x68:
		printf("LD   IXL,B");
		break;
	case 0x69:
		printf("LD   IXL,C");
		break;
	case 0x6A:
		printf("LD   IXL,D");
		break;
	case 0x6B:
		printf("LD   IXL,E");
		break;
	case 0x6C:
		printf("LD   IXL,IXH");
		break;
	case 0x6D:
		printf("LD   IXL,IXL");
		break;
	case 0x6E:
		printf("LD   L,(IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x6F:
		printf("LD   IXL,A");
		break;
	case 0x70:
		printf("LD   (IX+%02X),B", opcode[1]);
		*opbytes += 1;
		break;
	case 0x71:
		printf("LD   (IX+%02X),C", opcode[1]);
		*opbytes += 1;
		break;
	case 0x72:
		printf("LD   (IX+%02X),D", opcode[1]);
		*opbytes += 1;
		break;
	case 0x73:
		printf("LD   (IX+%02X),E", opcode[1]);
		*opbytes += 1;
		break;
	case 0x74:
		printf("LD   (IX+%02X),H", opcode[1]);
		*opbytes += 1;
		break;
	case 0x75:
		printf("LD   (IX+%02X),L", opcode[1]);
		*opbytes += 1;
		break;
	case 0x77:
		printf("LD   (IX+%02X),A", opcode[1]);
		*opbytes += 1;
		break;
	case 0x7C:
		printf("LD   A,IXH");
		break;
	case 0x7D:
		printf("LD   A,IXL");
		break;
	case 0x7E:
		printf("LD   A,(IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x84:
		printf("ADD  A,IXH");
		break;
	case 0x85:
		printf("ADD  A,IXL");
		break;
	case 0x86:
		printf("ADD  A,(IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x8C:
		printf("ADC  A,IXH");
		break;
	case 0x8D:
		printf("ADC  A,IXL");
		break;
	case 0x8E:
		printf("ADC  A,(IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x94:
		printf("SUB  A,IXH");
		break;
	case 0x95:
		printf("SUB  A,IXL");
		break;
	case 0x96:
		printf("SUB  A,(IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x9C:
		printf("SBC  A,IXH");
		break;
	case 0x9D:
		printf("SBC  A,IXL");
		break;
	case 0x9E:
		printf("SBC  A,(IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0xA4:
		printf("AND  IXH");
		break;
	case 0xA5:
		printf("AND  IXL");
		break;
	case 0xA6:
		printf("AND  (IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0xAC:
		printf("XOR  IXH");
		break;
	case 0xAD:
		printf("XOR  IXL");
		break;
	case 0xAE:
		printf("XOR  (IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0xB4:
		printf("OR   IXH");
		break;
	case 0xB5:
		printf("OR   IXL");
		break;
	case 0xB6:
		printf("OR   (IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0xBC:
		printf("CP   IXH");
		break;
	case 0xBD:
		printf("CP   IXL");
		break;
	case 0xBE:
		printf("CP   (IX+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0xCB:
		disassemble_ix_bits(&opcode[1], opbytes);
		break;
	case 0xE1:
		printf("POP  IX");
		break;
	case 0xE3:
		printf("EX   (SP),IX");
		break;
	case 0xE5:
		printf("PUSH IX");
		break;
	case 0xE9:
		printf("JP   (IX)");
		break;
	case 0xF9:
		printf("LD   SP,IX");
		break;
	default:
		printf("Invalid instruction...");
		*opbytes = -1;
		break;
	}
}

/** \brief Disassemble the IX bit instructions
 *  \param[in] opcode pointer to opcode to disassemble
 *  \param[out] opbytes pointer to int containing length of disassembled opcode
 */

static void disassemble_ix_bits(unsigned char *opcode, int *opbytes) {
	/*
	 * These opcodes are in the form of DDCB**NN where ** is the user
	 * provided number, and NN is the instruction. Due to this, we switch
	 * on opcode[1], and use opcode[0] as the operand.
	 */
	switch(opcode[1]) {
	case 0x00:
		printf("RLC  (IX+%02X),B", *opcode);
		break;
	case 0x01:
		printf("RLC  (IX+%02X),C", *opcode);
		break;
	case 0x02:
		printf("RLC  (IX+%02X),D", *opcode);
		break;
	case 0x03:
		printf("RLC  (IX+%02X),E", *opcode);
		break;
	case 0x04:
		printf("RLC  (IX+%02X),H", *opcode);
		break;
	case 0x05:
		printf("RLC  (IX+%02X),L", *opcode);
		break;
	case 0x06:
		printf("RLC  (IX+%02X)", *opcode);
		break;
	case 0x07:
		printf("RLC  (IX+%02X),A", *opcode);
		break;
	case 0x08:
		printf("RRC  (IX+%02X),B", *opcode);
		break;
	case 0x09:
		printf("RRC  (IX+%02X),C", *opcode);
		break;
	case 0x0A:
		printf("RRC  (IX+%02X),D", *opcode);
		break;
	}

	*opbytes += 1;
}

/** \brief Disassemble the bit instructions
 *  \param[in] opcode pointer to opcode to disassemble
 */

static void disassemble_bits(unsigned char *opcode) {
	switch(*opcode) {
	case 0x00:
		printf("RLC  B");
		break;
	case 0x01:
		printf("RLC  C");
		break;
	case 0x02:
		printf("RLC  D");
		break;
	case 0x03:
		printf("RLC  E");
		break;
	case 0x04:
		printf("RLC  H");
		break;
	case 0x05:
		printf("RLC  L");
		break;
	case 0x06:
		printf("RLC  (HL)");
		break;
	case 0x07:
		printf("RLC  A");
		break;
	case 0x08:
		printf("RRC  B");
		break;
	case 0x09:
		printf("RRC  C");
		break;
	case 0x0A:
		printf("RRC  D");
		break;
	case 0x0B:
		printf("RRC  E");
		break;
	case 0x0C:
		printf("RRC  H");
		break;
	case 0x0D:
		printf("RRC  L");
		break;
	case 0x0E:
		printf("RRC  (HL)");
		break;
	case 0x0F:
		printf("RRC  A");
		break;
	case 0x10:
		printf("RL   B");
		break;
	case 0x11:
		printf("RL   C");
		break;
	case 0x12:
		printf("RL   D");
		break;
	case 0x13:
		printf("RL   E");
		break;
	case 0x14:
		printf("RL   H");
		break;
	case 0x15:
		printf("RL   L");
		break;
	case 0x16:
		printf("RL   (HL)");
		break;
	case 0x17:
		printf("RL   A");
		break;
	case 0x18:
		printf("RR   B");
		break;
	case 0x19:
		printf("RR   C");
		break;
	case 0x1A:
		printf("RR   D");
		break;
	case 0x1B:
		printf("RR   E");
		break;
	case 0x1C:
		printf("RR   H");
		break;
	case 0x1D:
		printf("RR   L");
		break;
	case 0x1E:
		printf("RR   (HL)");
		break;
	case 0x1F:
		printf("RR   A");
		break;
	case 0x20:
		printf("SLA  B");
		break;
	case 0x21:
		printf("SLA  C");
		break;
	case 0x22:
		printf("SLA  D");
		break;
	case 0x23:
		printf("SLA  E");
		break;
	case 0x24:
		printf("SLA  H");
		break;
	case 0x25:
		printf("SLA  L");
		break;
	case 0x26:
		printf("SLA  (HL)");
		break;
	case 0x27:
		printf("SLA  A");
		break;
	case 0x28:
		printf("SRA  B");
		break;
	case 0x29:
		printf("SRA  C");
		break;
	case 0x2A:
		printf("SRA  D");
		break;
	case 0x2B:
		printf("SRA  E");
		break;
	case 0x2C:
		printf("SRA  H");
		break;
	case 0x2D:
		printf("SRA  L");
		break;
	case 0x2E:
		printf("SRA  (HL)");
		break;
	case 0x2F:
		printf("SRA  A");
		break;
	case 0x30:
		printf("SLL  B");
		break;
	case 0x31:
		printf("SLL  C");
		break;
	case 0x32:
		printf("SLL  D");
		break;
	case 0x33:
		printf("SLL  E");
		break;
	case 0x34:
		printf("SLL  H");
		break;
	case 0x35:
		printf("SLL  L");
		break;
	case 0x36:
		printf("SLL  (HL)");
		break;
	case 0x37:
		printf("SLL  A");
		break;
	case 0x38:
		printf("SRL  B");
		break;
	case 0x39:
		printf("SRL  C");
		break;
	case 0x3A:
		printf("SRL  D");
		break;
	case 0x3B:
		printf("SRL  E");
		break;
	case 0x3C:
		printf("SRL  H");
		break;
	case 0x3D:
		printf("SRL  L");
		break;
	case 0x3E:
		printf("SRL  (HL)");
		break;
	case 0x3F:
		printf("SRL  A");
		break;
	case 0x40:
		printf("BIT  0,B");
		break;
	case 0x41:
		printf("BIT  0,C");
		break;
	case 0x42:
		printf("BIT  0,D");
		break;
	case 0x43:
		printf("BIT  0,E");
		break;
	case 0x44:
		printf("BIT  0,H");
		break;
	case 0x45:
		printf("BIT  0,L");
		break;
	case 0x46:
		printf("BIT  0,(HL)");
		break;
	case 0x47:
		printf("BIT  0,A");
		break;
	case 0x48:
		printf("BIT  1,B");
		break;
	case 0x49:
		printf("BIT  1,C");
		break;
	case 0x4A:
		printf("BIT  1,D");
		break;
	case 0x4B:
		printf("BIT  1,E");
		break;
	case 0x4C:
		printf("BIT  1,H");
		break;
	case 0x4D:
		printf("BIT  1,L");
		break;
	case 0x4E:
		printf("BIT  1,(HL)");
		break;
	case 0x4F:
		printf("BIT  1,A)");
		break;
	case 0x50:
		printf("BIT  2,B");
		break;
	case 0x51:
		printf("BIT  2,C");
		break;
	case 0x52:
		printf("BIT  2,D");
		break;
	case 0x53:
		printf("BIT  2,E");
		break;
	case 0x54:
		printf("BIT  2,H");
		break;
	case 0x55:
		printf("BIT  2,L");
		break;
	case 0x56:
		printf("BIT  2,(HL)");
		break;
	case 0x57:
		printf("BIT  2,A");
		break;
	case 0x58:
		printf("BIT  3,B");
		break;
	case 0x59:
		printf("BIT  3,C");
		break;
	case 0x5A:
		printf("BIT  3,D");
		break;
	case 0x5B:
		printf("BIT  3,E");
		break;
	case 0x5C:
		printf("BIT  3,H");
		break;
	case 0x5D:
		printf("BIT  3,L");
		break;
	case 0x5E:
		printf("BIT  3,(HL)");
		break;
	case 0x5F:
		printf("BIT  3,A");
		break;
	case 0x60:
		printf("BIT  4,B");
		break;
	case 0x61:
		printf("BIT  4,C");
		break;
	case 0x62:
		printf("BIT  4,D");
		break;
	case 0x63:
		printf("BIT  4,E");
		break;
	case 0x64:
		printf("BIT  4,H");
		break;
	case 0x65:
		printf("BIT  4,L");
		break;
	case 0x66:
		printf("BIT  4,(HL)");
		break;
	case 0x67:
		printf("BIT  4,A");
		break;
	case 0x68:
		printf("BIT  5,B");
		break;
	case 0x69:
		printf("BIT  5,C");
		break;
	case 0x6A:
		printf("BIT  5,D");
		break;
	case 0x6B:
		printf("BIT  5,E");
		break;
	case 0x6C:
		printf("BIT  5,H");
		break;
	case 0x6D:
		printf("BIT  5,L");
		break;
	case 0x6E:
		printf("BIT  5,(HL)");
		break;
	case 0x6F:
		printf("BIT  5,A");
		break;
	case 0x70:
		printf("BIT  6,B");
		break;
	case 0x71:
		printf("BIT  6,C");
		break;
	case 0x72:
		printf("BIT  6.D");
		break;
	case 0x73:
		printf("BIT  6,E");
		break;
	case 0x74:
		printf("BIT  6,H");
		break;
	case 0x75:
		printf("BIT  6,L");
		break;
	case 0x76:
		printf("BIT  6,(HL)");
		break;
	case 0x77:
		printf("BIT  6,A");
		break;
	case 0x78:
		printf("BIT  7,B");
		break;
	case 0x79:
		printf("BIT  7,C");
		break;
	case 0x7A:
		printf("BIT  7,D");
		break;
	case 0x7B:
		printf("BIT  7,E");
		break;
	case 0x7C:
		printf("BIT  7,H");
		break;
	case 0x7D:
		printf("BIT  7,L");
		break;
	case 0x7E:
		printf("BIT  7,(HL)");
		break;
	case 0x7F:
		printf("BIT  7,A");
		break;
	case 0x80:
		printf("RES  0,B");
		break;
	case 0x81:
		printf("RES  0,C");
		break;
	case 0x82:
		printf("RES  0,D");
		break;
	case 0x83:
		printf("RES  0,E");
		break;
	case 0x84:
		printf("RES  0,H");
		break;
	case 0x85:
		printf("RES  0,L");
		break;
	case 0x86:
		printf("RES  0,(HL)");
		break;
	case 0x87:
		printf("RES  0,A");
		break;
	case 0x88:
		printf("RES  1,B");
		break;
	case 0x89:
		printf("RES  1,C");
		break;
	case 0x8A:
		printf("RES  1,D");
		break;
	case 0x8B:
		printf("RES  1,E");
		break;
	case 0x8C:
		printf("RES  1,H");
		break;
	case 0x8D:
		printf("RES  1,L");
		break;
	case 0x8E:
		printf("RES  1,(HL)");
		break;
	case 0x8F:
		printf("RES  1,A");
		break;
	case 0x90:
		printf("RES  2,B");
		break;
	case 0x91:
		printf("RES  2,C");
		break;
	case 0x92:
		printf("RES  2,D");
		break;
	case 0x93:
		printf("RES  2,E");
		break;
	case 0x94:
		printf("RES  2,H");
		break;
	case 0x95:
		printf("RES  2,L");
		break;
	case 0x96:
		printf("RES  2,(HL)");
		break;
	case 0x97:
		printf("RES  2,A");
		break;
	case 0x98:
		printf("RES  3,B");
		break;
	case 0x99:
		printf("RES  3,C");
		break;
	case 0x9A:
		printf("RES  3,D");
		break;
	case 0x9B:
		printf("RES  3,E");
		break;
	case 0x9C:
		printf("RES  3,H");
		break;
	case 0x9D:
		printf("RES  3,L");
		break;
	case 0x9E:
		printf("RES  3,(HL)");
		break;
	case 0x9F:
		printf("RES  3,A");
		break;
	case 0xA0:
		printf("RES  4,B");
		break;
	case 0xA1:
		printf("RES  4,C");
		break;
	case 0xA2:
		printf("RES  4,D");
		break;
	case 0xA3:
		printf("RES  4,E");
		break;
	case 0xA4:
		printf("RES  4,H");
		break;
	case 0xA5:
		printf("RES  4,L");
		break;
	case 0xA6:
		printf("RES  4,(HL)");
		break;
	case 0xA7:
		printf("RES  4,A");
		break;
	case 0xA8:
		printf("RES  5,B");
		break;
	case 0xA9:
		printf("RES  5,C");
		break;
	case 0xAA:
		printf("RES  5,D");
		break;
	case 0xAB:
		printf("RES  5,E");
		break;
	case 0xAC:
		printf("RES  5,H");
		break;
	case 0xAD:
		printf("RES  5,L");
		break;
	case 0xAE:
		printf("RES  5,(HL)");
		break;
	case 0xAF:
		printf("RES  5,A");
		break;
	case 0xB0:
		printf("RES  6,B");
		break;
	case 0xB1:
		printf("RES  6,C");
		break;
	case 0xB2:
		printf("RES  6,D");
		break;
	case 0xB3:
		printf("RES  6,E");
		break;
	case 0xB4:
		printf("RES  6,H");
		break;
	case 0xB5:
		printf("RES  6,L");
		break;
	case 0xB6:
		printf("RES  6,(HL)");
		break;
	case 0xB7:
		printf("RES  6,A");
		break;
	case 0xB8:
		printf("RES  7,B");
		break;
	case 0xB9:
		printf("RES  7,C");
		break;
	case 0xBA:
		printf("RES  7,D");
		break;
	case 0xBB:
		printf("RES  7,E");
		break;
	case 0xBC:
		printf("RES  7,H");
		break;
	case 0xBD:
		printf("RES  7,L");
		break;
	case 0xBE:
		printf("RES  7,(HL)");
		break;
	case 0xBF:
		printf("RES  7,A");
		break;
	case 0xC0:
		printf("SET  0,B");
		break;
	case 0xC1:
		printf("SET  0,C");
		break;
	case 0xC2:
		printf("SET  0,D");
		break;
	case 0xC3:
		printf("SET  0,E");
		break;
	case 0xC4:
		printf("SET  0,H");
		break;
	case 0xC5:
		printf("SET  0,L");
		break;
	case 0xC6:
		printf("SET  0,(HL)");
		break;
	case 0xC7:
		printf("SET  0,A");
		break;
	case 0xC8:
		printf("SET  1,B");
		break;
	case 0xC9:
		printf("SET  1,C");
		break;
	case 0xCA:
		printf("SET  1,D");
		break;
	case 0xCB:
		printf("SET  1,E");
		break;
	case 0xCC:
		printf("SET  1,H");
		break;
	case 0xCD:
		printf("SET  1,L");
		break;
	case 0xCE:
		printf("SET  1,(HL)");
		break;
	case 0xCF:
		printf("SET  1,A");
		break;
	case 0xD0:
		printf("SET  2,B");
		break;
	case 0xD1:
		printf("SET  2,C");
		break;
	case 0xD2:
		printf("SET  2,D");
		break;
	case 0xD3:
		printf("SET  2,E");
		break;
	case 0xD4:
		printf("SET  2,H");
		break;
	case 0xD5:
		printf("SET  2,L");
		break;
	case 0xD6:
		printf("SET  2,(HL)");
		break;
	case 0xD7:
		printf("SET  2,A");
		break;
	case 0xD8:
		printf("SET  3,B");
		break;
	case 0xD9:
		printf("SET  3,C");
		break;
	case 0xDA:
		printf("SET  3,D");
		break;
	case 0xDB:
		printf("SET  3,E");
		break;
	case 0xDC:
		printf("SET  3,H");
		break;
	case 0xDD:
		printf("SET  3,L");
		break;
	case 0xDE:
		printf("SET  3,(HL)");
		break;
	case 0xDF:
		printf("SET  3,A");
		break;
	case 0xE0:
		printf("SET  4,B");
		break;
	case 0xE1:
		printf("SET  4,C");
		break;
	case 0xE2:
		printf("SET  4,D");
		break;
	case 0xE3:
		printf("SET  4,E");
		break;
	case 0xE4:
		printf("SET  4,H");
		break;
	case 0xE5:
		printf("SET  4,L");
		break;
	case 0xE6:
		printf("SET  4,(HL)");
		break;
	case 0xE7:
		printf("SET  4,A");
		break;
	case 0xE8:
		printf("SET  5,B");
		break;
	case 0xE9:
		printf("SET  5,C");
		break;
	case 0xEA:
		printf("SET  5,D");
		break;
	case 0xEB:
		printf("SET  5,E");
		break;
	case 0xEC:
		printf("SET  5,H");
		break;
	case 0xED:
		printf("SET  5,L");
		break;
	case 0xEE:
		printf("SET  5,(HL)");
		break;
	case 0xEF:
		printf("SET  5,A");
		break;
	case 0xF0:
		printf("SET  6,B");
		break;
	case 0xF1:
		printf("SET  6,C");
		break;
	case 0xF2:
		printf("SET  6,D");
		break;
	case 0xF3:
		printf("SET  6,E");
		break;
	case 0xF4:
		printf("SET  6,H");
		break;
	case 0xF5:
		printf("SET  6,L");
		break;
	case 0xF6:
		printf("SET  6,(HL)");
		break;
	case 0xF7:
		printf("SET  6,A");
		break;
	case 0xF8:
		printf("SET  7,B");
		break;
	case 0xF9:
		printf("SET  7,C");
		break;
	case 0xFA:
		printf("SET  7,D");
		break;
	case 0xFB:
		printf("SET  7,E");
		break;
	case 0xFC:
		printf("SET  7,H");
		break;
	case 0xFD:
		printf("SET  7,L");
		break;
	case 0xFE:
		printf("SET  7,(HL)");
		break;
	case 0xFF:
		printf("SET  7,A");
		break;
	}
}

/** \brief Disassemble the IY instructions
 *  \param[in] opcode pointer to opcode to disassemble
 *  \param[out] opbytes pointer to int containing length of disassembled opcode
 */

static void disassemble_iy(unsigned char *opcode, int *opbytes) {
	switch(*opcode) {            /* these are the IY instructions */
	case 0x09:
		printf("ADD  IY,BC");
		break;
	case 0x19:
		printf("ADD  IY,DE");
		break;
	case 0x21:
		printf("LD   IY,%02X%02X", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x22:
		printf("LD   (%02X%02X),IY", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x23:
		printf("INC  IY");
		break;
	case 0x24:
		printf("INC  IYH");
		break;
	case 0x25:
		printf("DEC  IYH");
		break;
	case 0x26:
		printf("LD   IYH,%02X", opcode[1]);
		*opbytes += 1;
		break;
	case 0x29:
		printf("ADD  IY,IY");
		break;
	case 0x2A:
		printf("LD   IY,(%02X%02X)", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x2B:
		printf("INC  IYL");
		break;
	case 0x2C:
		printf("DEC  IYL");
		break;
	case 0x2D:
		printf("LD   IYL,%02X", opcode[1]);
		*opbytes += 1;
		break;
	case 0x34:
		printf("INC  (IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x35:
		printf("DEC  (IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x36:
		printf("LD   (IY+%02X), %02X", opcode[2], opcode[1]);
		*opbytes += 2;
		break;
	case 0x39:
		printf("ADD  IY,SP");
		break;
	case 0x44:
		printf("LD   B,YXH");
		break;
	case 0x45:
		printf("LD   B,YXL");
		break;
	case 0x46:
		printf("LD   B,(IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x4C:
		printf("LD   C,IYH");
		break;
	case 0x4D:
		printf("LD   C,IYL");
		break;
	case 0x4E:
		printf("LD   C,(IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x54:
		printf("LD   D,IYH");
		break;
	case 0x55:
		printf("LD   D,IYL");
		break;
	case 0x56:
		printf("LD   D,(IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x5C:
		printf("LD   E,IYH");
		break;
	case 0x5D:
		printf("LD   E,IYL");
		break;
	case 0x5E:
		printf("LD   E,(IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x60:
		printf("LD   IYH,B");
		break;
	case 0x61:
		printf("LD   IYH,C");
		break;
	case 0x62:
		printf("LD   IYH,D");
		break;
	case 0x63:
		printf("LD   IYH,E");
		break;
	case 0x64:
		printf("LD   IYH,IXH");
		break;
	case 0x65:
		printf("LD   IYH,IXL");
		break;
	case 0x66:
		printf("LD   H,(IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x67:
		printf("LD   IYH,A");
		break;
	case 0x68:
		printf("LD   IYL,B");
		break;
	case 0x69:
		printf("LD   IYL,C");
		break;
	case 0x6A:
		printf("LD   IYL,D");
		break;
	case 0x6B:
		printf("LD   IYL,E");
		break;
	case 0x6C:
		printf("LD   IYL,IXH");
		break;
	case 0x6D:
		printf("LD   IYL,IXL");
		break;
	case 0x6E:
		printf("LD   L,(IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x6F:
		printf("LD   IYL,A");
		break;
	case 0x70:
		printf("LD   (IY+%02X),B", opcode[1]);
		*opbytes += 1;
		break;
	case 0x71:
		printf("LD   (IY+%02X),C", opcode[1]);
		*opbytes += 1;
		break;
	case 0x72:
		printf("LD   (IY+%02X),D", opcode[1]);
		*opbytes += 1;
		break;
	case 0x73:
		printf("LD   (IY+%02X),E", opcode[1]);
		*opbytes += 1;
		break;
	case 0x74:
		printf("LD   (IY+%02X),H", opcode[1]);
		*opbytes += 1;
		break;
	case 0x75:
		printf("LD   (IY+%02X),L", opcode[1]);
		*opbytes += 1;
		break;
	case 0x77:
		printf("LD   (IY+%02X),A", opcode[1]);
		*opbytes += 1;
		break;
	case 0x7C:
		printf("LD   A,IYH");
		break;
	case 0x7D:
		printf("LD   A,IYL");
		break;
	case 0x7E:
		printf("LD   A,(IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x84:
		printf("ADD  A,IYH");
		break;
	case 0x85:
		printf("ADD  A,IYL");
		break;
	case 0x86:
		printf("ADD  A,(IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x8C:
		printf("ADC  A,IYH");
		break;
	case 0x8D:
		printf("ADC  A,IYL");
		break;
	case 0x8E:
		printf("ADC  A,(IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x94:
		printf("SUB  A,IYH");
		break;
	case 0x95:
		printf("SUB  A,IYL");
		break;
	case 0x96:
		printf("SUB  A,(IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0x9C:
		printf("SBC  A,IYH");
		break;
	case 0x9D:
		printf("SBC  A,IYL");
		break;
	case 0x9E:
		printf("SBC  A,(IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0xA4:
		printf("AND  IYH");
		break;
	case 0xA5:
		printf("AND  IYL");
		break;
	case 0xA6:
		printf("AND  (IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0xAC:
		printf("XOR  IYH");
		break;
	case 0xAD:
		printf("XOR  IYL");
		break;
	case 0xAE:
		printf("XOR  (IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0xB4:
		printf("OR   IYH");
		break;
	case 0xB5:
		printf("OR   IYL");
		break;
	case 0xB6:
		printf("OR   (IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0xBC:
		printf("CP   IYH");
		break;
	case 0xBD:
		printf("CP   IYL");
		break;
	case 0xBE:
		printf("CP   (IY+%02X)", opcode[1]);
		*opbytes += 1;
		break;
	case 0xCB:
		disassemble_iy_bits(&opcode[1], opbytes);
		break;
	case 0xE1:
		printf("POP  IY");
		break;
	case 0xE3:
		printf("EX   (SP),IY");
		break;
	case 0xE5:
		printf("PUSH IY");
		break;
	case 0xE9:
		printf("JP   (IY)");
		break;
	case 0xF9:
		printf("LD   SP,IY");
		break;
	default:
		printf("Invalid instruction...");
		*opbytes = -1;
		break;
	}
}

/** \brief Disassemble the IY bit instructions
 *  \param[in] opcode pointer to opcode to disassemble
 *  \param[out] opbytes pointer to int containing length of disassembled opcode
 */

static void disassemble_iy_bits(unsigned char *opcode, int *opbytes) {
	/*
	 * These opcodes are in the form of DDCB**NN where ** is the user
	 * provided number, and NN is the instruction. Due to this, we switch
	 * on opcode[1], and use opcode[0] as the operand.
	 */
	switch(opcode[1]) {
	case 0x00:
		printf("RLC  (IY+%02X),B", *opcode);
		break;
	case 0x01:
		printf("RLC  (IY+%02X),C", *opcode);
		break;
	case 0x02:
		printf("RLC  (IY+%02X),D", *opcode);
		break;
	case 0x03:
		printf("RLC  (IY+%02X),E", *opcode);
		break;
	case 0x04:
		printf("RLC  (IY+%02X),H", *opcode);
		break;
	case 0x05:
		printf("RLC  (IY+%02X),L", *opcode);
		break;
	case 0x06:
		printf("RLC  (IY+%02X)", *opcode);
		break;
	case 0x07:
		printf("RLC  (IY+%02X),A", *opcode);
		break;
	case 0x08:
		printf("RRC  (IY+%02X),B", *opcode);
		break;
	case 0x09:
		printf("RRC  (IY+%02X),C", *opcode);
		break;
	case 0x0A:
		printf("RRC  (IY+%02X),D", *opcode);
		break;
	}

	*opbytes += 1;
}
